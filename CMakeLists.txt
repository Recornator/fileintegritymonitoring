cmake_minimum_required(VERSION 2.8)

project(integrityMonitoring)

set(SOURCE_EXE main.cpp)

set(SOURCE_LIB source.cpp)

add_library(hashFunctions STATIC ${SOURCE_LIB})

add_executable(DirectoryIntegrityMonitoring ${SOURCE_EXE})

target_link_libraries(DirectoryIntegrityMonitoring hashFunctions)

