#include "includes.h"
#include <iomanip>
#include <limits>

int main(){
    int response = 0;
    std::string directory, filelist, quietmode;

    while (response != 3){
        std::cout << std::string(50, '-') << std::endl;
        std::cout << std::setw(40) << "Directory integrity monitoring v1.1" << std::endl;
        std::cout << "Select one of the options below:" << std::endl;
        std::cout << "1) Create file list of selected directory" << std::endl;
        std::cout << "2) Verify integrity of selected directory" << std::endl;
        std::cout << "3) Exit" << std::endl;
        std::cout << std::string(50, '-') << std::endl;

        if (!(std::cin >> response)) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<int>::max(), '\n');
            std::cout << "Enter number between 1 to 3" << std::endl;
            continue;
        }

        switch (response) {
            case 1:
                std::cout << "Option 1 selected" << std::endl;
                std::cout << "Enter path to directory:" <<std::endl;
                std::cin >> directory;
                std::cout << "Enter path where to create file list:" <<std::endl;
                std::cin >> filelist;
                if (filelist.find(directory) != -1){
                    std::cout << "WARNING: future behavior may not be correct as you save file list into the same directory" << std::endl;
                }
                directory_files_to_filelist(directory,filelist);
                break;
            case 2:
                std::cout << "Option 2 selected" << std::endl;
                std::cout << "Enter path to directory:" <<std::endl;
                std::cin >> directory;
                std::cout << "Enter path where to create file list:" <<std::endl;
                std::cin >> filelist;
                std::cout << "Do you want to turn on quiet mode? (Display only wrong files) [yes/no]" << std::endl;
                std::cin >> quietmode;
                if (quietmode == "yes") integrity_check(directory,filelist, true);
                else integrity_check(directory,filelist, false);
                break;
            case 3:
                std::cout << "Exiting program" << std::endl;
                break;
            default:
                std::cout << "Wrong number entered, select number between 1 to 3" << std::endl;
                break;
        }
    }
    return 0;
}
