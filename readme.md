### Directory Integrity Monitoring program
Use:
1) Create file list which contains all files of given directory (recursively) and their hash-code.
2) Verify directory with given file list, it detects:
    * Changes with file
    * Deleted files listed in file list
    * Files which are not listed in file list

__It is not recommended to save file list into the same directory__