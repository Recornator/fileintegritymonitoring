#pragma once
#include <iostream>

class FileListBadPathException;

void directory_files_to_filelist(const std::string&, const std::string&);

void integrity_check(const std::string&, const std::string&, bool);
