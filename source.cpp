#include "includes.h"

#include <fstream>
#include <vector>
#include <string_view>
#include <filesystem>
#include <algorithm>

const std::string nameOfFileList = "FileList";
const uint16_t chunkSize = 1024;
const char delimiter = '|';

class FileListBadPathException: public std::exception{
public:
    const char * what() const noexcept override{
        return "Incorrect path to file list";
    }
};

struct fileobject{
    std::string path;
    std::size_t hash;
};

template <class T>
inline void hash_combine(std::size_t& seed, const T& v){
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

std::size_t calculate_hash_of_file(const std::string& filename){
    std::ifstream input(filename);

    std::streamsize s;
    std::size_t hash = 0;
    char buffer[chunkSize] = {};

    while (!input.eof()){
        input.read(buffer, 1024);

        s = input.gcount();

        hash_combine( hash, std::hash< std::string_view >{}( std::string_view( buffer, s) ));
    }
    input.close();

    return hash;

}

bool compare_hash_of_file(const std::string& filename, std::size_t filehash){
    return (filehash == calculate_hash_of_file(filename));
}

void directory_files_to_filelist(const std::string& dirname,const std::string& filelistpath){
    try {
        long sizeOfPath = 0;
        std::size_t hash = 0;

        std::ofstream filelist(filelistpath + "/" + nameOfFileList, std::ios_base::binary);
        if (!filelist){
            throw FileListBadPathException{};
        }
        for (std::filesystem::recursive_directory_iterator i(dirname), end; i != end; i++) {
            if (!std::filesystem::is_directory(i->path())) {
                sizeOfPath = i->path().string().size();
                filelist.write(reinterpret_cast<char*>(&sizeOfPath), sizeof(sizeOfPath));
                filelist.write(i->path().string().data(),sizeOfPath);
                hash = calculate_hash_of_file(i->path());
                filelist.write(reinterpret_cast<char*>(&hash), sizeof(hash));
            }
        }
        filelist.close();
        std::cout << "File list created" << std::endl;
    }
    catch(const std::filesystem::filesystem_error& e) {
        std::cout << "Incorrect path to directory" << std::endl;
    }
    catch(FileListBadPathException& e){
        std::cout << e.what() << std::endl;
    }
}

std::vector<fileobject> deserialize(const std::string& filelistpath){
    try{
        std::ifstream filelist(filelistpath + "/" + nameOfFileList, std::ios_base::binary);
        if (!filelist){
            throw FileListBadPathException{};
        }

        std::vector<fileobject> revision;
        std::string line;
        std::size_t hash;
        long sizeOfPath;

        while (true){
            filelist.read( reinterpret_cast<char*>(&sizeOfPath), sizeof(sizeOfPath) );
            if (filelist.eof() ){
                break;
            }
            line.resize(sizeOfPath);
            filelist.read( reinterpret_cast<char*>(line.data()), sizeOfPath );
            filelist.read( reinterpret_cast<char*>(&hash), sizeof(hash) );
            revision.push_back( fileobject{line, hash } );
        }

        filelist.close();
        return revision;
    }
    catch (const FileListBadPathException& e){
        throw;
    }
}

void revision_check(const std::string& dirname, std::vector<fileobject>& revision, bool quiet){
    bool fileFound = false;
    bool equal = false;
    bool noerrors = true;

    for (std::filesystem::recursive_directory_iterator i(dirname), end; i != end; i++) {
        if (!std::filesystem::is_directory(i->path())) {
            if (!quiet) std::cout << i->path().string() << std::endl;
            for(int j=0; j<revision.size();j++){
                if ( revision[j].path == i->path().string() ){
                    fileFound = true;
                    equal = compare_hash_of_file( i->path().string(), revision[j].hash );
                    revision.erase (revision.begin() + j);
                    break;
                }
            }
            if (!fileFound){
                noerrors = false;
                if (quiet) std::cout << i->path().string() << std::endl;
                std::cout << "This file is not in file list" <<std::endl;
                std::cout << "----------" << std::endl;
            }
            else if (!equal){
                noerrors = false;
                if (quiet) std::cout << i->path().string() << std::endl;
                std::cout << "This file was changed" <<std::endl;
                std::cout << "----------" << std::endl;
            }
            else if (!quiet){
                std::cout << "OK" << std::endl;
                std::cout << "----------" << std::endl;
            }
            fileFound = false;
        }
    }
    if (!revision.empty()){
        noerrors = false;
        std::cout << "Following files were deleted:" << std::endl;
        for (const auto& j: revision) std::cout << j.path << std::endl;
    }
    if(noerrors) {
        std::cout << "All files are OK" << std::endl;
    }
}

void integrity_check(const std::string& dirname, const std::string& filelistpath, bool quiet = false){
    try {
        std::vector<fileobject> revision = deserialize(filelistpath);

        revision_check(dirname, revision, quiet);

        std::cout << "Integrity check completed" << std::endl;
    }
    catch(const std::filesystem::filesystem_error& e) {
        std::cout<< "Incorrect path to directory" << std::endl;
    }
    catch(FileListBadPathException& e){
        std::cout<< e.what() << std::endl;
    }
}
